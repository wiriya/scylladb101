> To see the snitch defined in our multi-dc cluster, we use the describecluster command:
docker exec -it scylla-node1 nodetool describecluster

> We can see that the Snitch being used is the GossipingPropertyFileSnitch. It allows us to explicitly define which DC and Rack a specific Node belongs to. This snitch reads its configuration from a cassandra-rackdc.properties file located under /etc/scylla/
docker exec -it  scylla-node1 cat /etc/scylla/cassandra-rackdc.properties